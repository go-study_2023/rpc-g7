# 如何生成代码

```sh
# 在 protobuf/pb 文件夹下面
$ protoc -I=. --go_out=. --go_opt=module="gitee.com/go-course/rpc-g7/protobuf/pb"   ./hello.proto

# 在 protobuf/pb 文件夹下面, 为所有以proto结尾的文件 做编译
protoc -I=. --go_out=. --go_opt=module="gitee.com/go-course/rpc-g7/protobuf/pb"   ./*.proto

# 在 protobuf 文件夹下面
$ protoc -I=./pb/ --go_out=./pb/ --go_opt=module="gitee.com/go-course/rpc-g7/protobuf/pb"  hello.proto

# 在 protobuf 文件夹下面
$ protoc -I=./pb/ --go_out=./pb/ --go_opt=module="gitee.com/go-course/rpc-g7/protobuf/pb"  hello.proto

# 引入了Any 后 需要通过-I 指定any 类型的文件目录
$ protoc -I=./pb/ -I=/usr/local/include --go_out=./pb/ --go_opt=module="gitee.com/go-course/rpc-g7/protobuf/pb"  hello.proto
```