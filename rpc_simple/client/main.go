package main

import (
	"fmt"
	"net/rpc"
)

func main() {
	// 1. 建立socket连接
	client, err := rpc.Dial("tcp", ":1234")
	if err != nil {
		panic(err)
	}

	// 执行RPC方法
	var resp string
	err = client.Call("HelloService.Hello", 10, &resp)
	if err != nil {
		panic(err)
	}

	// 打印调用响应
	fmt.Println(resp)
}
